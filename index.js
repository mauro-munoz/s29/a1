const e = require('express');
const express =require('express');
const app = express();
const port = 3000;

app.use(express.json())
app.use(express.urlencoded({extended:true}))

users = [
    {"username":"Mauro","password":"123password"},
    {"username":"Mark","password":"123123"}
];
app.get('/home', (req, res) => {
    res.send('Welcome to Homepage')
  })

app.get(`/users`, (req,res) => {
    res.send(users)
})
  
app.delete(`/delete-user`, (req,res) => {

    const {username} = req.body;
    const deleted = users.find((users) => users.username === username)

    if (deleted) {
        users.splice(users.username - 1,1)
                res.send(`user ${username} has been deleted`)
                console.log(users)
    } else {res.send(`User ${username} cannot be found`)}

    // for (i = 0; i >= users.length; i++){
    //     if(users[i].username == req.body.username){
    //         users.splice(i,1)
    //         res.send(`user ${req.body.username} has been deleted`)
    //         // console.log(users)
    //         break;
    //     } else {
    //         res.send(`User ${req.body.username} cannot be found`)
    //         break;
    //     }
    // }  
    
})

app.listen(port, () => console.log (`server is running at port ${port}`))